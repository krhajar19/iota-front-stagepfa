import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileAlt, faSpinner, faTrash } from '@fortawesome/free-solid-svg-icons'
import './FileItem.scss'

const FileItem = ({ files, deleteFile }) => {
    return (
        <>
            <li
                className="file-item"
                key={files.file.name}>
                <FontAwesomeIcon icon={faFileAlt} />
                <p>{files.file.name}</p>
                <div className="actions">
                    <div className="loading"></div>
                        <FontAwesomeIcon icon={faTrash}
                            onClick={() => deleteFile()} />
                </div>
            </li>
        </>
    )
}

export default FileItem
