import React from 'react'
import './App.scss';
import Success from './Success/Success';
import Upload from './upload/Upload';
import {BrowserRouter as Router ,Routes,Route} from "react-router-dom";

function App() {
  return (
       <Router>
            <Routes>
              <Route path='/' element={<Upload/>}></Route>
              <Route path='success' element={<Success/>}></Route>
            </Routes>
       </Router>
  );
}

export default App;
