import React, { useState } from "react";
import LoadingOverlay from 'react-loading-overlay';
import './loading.css';
import styled, { css } from "styled-components";
function LoadingFile({ loading }){

  const DarkBackground = styled.div`
  position: fixed; 
  z-index: 999;
  left: 0;
  top: 0;
  width: 100%; 
  height: 100%;
  overflow: auto;
  background-color: rgb(0, 0, 0);
  background-color: rgba(0, 0, 0, 0.4); 
`;
return (
  <div>
     <DarkBackground>
      <LoadingOverlay styles={{ content: base => ({
        ...base,
        color: "black"
      }),
      spinner : base => ({
        ...base,
        flex: 1,
        marginTop:300,
        justifyContent: 'center',
        alignItems:'center'
      })
    }} 
  active={loading}
spinner
  text='Loading...'
  >
</LoadingOverlay>
</DarkBackground>
  </div>
);
}
export default LoadingFile; 