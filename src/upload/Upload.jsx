import { useEffect, useState } from 'react'
import FileUpload from '../FileUpload/FileUpload';
import FileList from '../FileList/FileList';
import axios from 'axios';
import {useNavigate} from "react-router-dom";
import LoadingFile from '../loadingFile/LoadingFile';

function Upload() {
  const [files, setFiles] = useState(null);
  const [data, setData] = useState({});
  const [loading,setLoading]=useState(false);
  const navigate = useNavigate();
  const toComponentSuccess=()=>{
    navigate('/success',{state:{...data}});
      }
  useEffect(() => {
    if(Object.keys(data).length !== 0) {
      toComponentSuccess();
    }
  },[data]);

  const removeFile = () => {
    const file = undefined;
    setFiles(previous => (null));
  }
    // upload file
    const FileDone = e =>{
      setLoading(true)
      setFiles(previous => ({ ...previous, isUploading:true }))
  const formData = new FormData();
  formData.append(
      "resume",
      files.file,
      files.file.name
  )
   axios.post('http://localhost:8080/api/upload', formData)
      .then((res) => {
        const tempData = res.data.data;
        setData({ ...data, ...tempData });
        setFiles(previous => ({ ...previous, isUploading:false }));
        setLoading(false);
        //toComponentSuccess();
      })
      .catch((err) => {
          // inform the user
          console.error(err)
          // removeFile(files.file.name);
      });
 
}

  return (
   <>
    <div className="title">welcome to E-challange platform,plase uplode your cv </div>
    <div>
      <FileUpload files={files} setFiles={setFiles}
        removeFile={removeFile} />
      <FileList files={files} removeFile={removeFile} />
      </div>
      <div> 
                
                <button  onClick={FileDone}  className="file-done">
                  Done
                </button>
        </div>
    {loading && <LoadingFile loading={loading} />}
    
    </>
  );
}

export default Upload;