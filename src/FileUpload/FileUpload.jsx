import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import './FileUpload.scss'
// import axios from 'axios'
const FileUpload = ({ files, setFiles, removeFile }) => {
    const uploadHandler = (event) => {
        console.log("test");
        const file = event.target.files[0];
        if(!file) return;
        const isUploading = false;
        setFiles(previous => ({ ...previous, file }));
    }
    //     // upload file
    //     const FileDone = e =>{
    //         setFiles(previous => ({ ...previous, isUploading:true }))
    //     const formData = new FormData();
    //     formData.append(
    //         "newFile",
    //         files.file,
    //         files.file.name
    //     )
    //      axios.post('http://localhost:8081/upload', formData)
    //         .then((res) => {
    //             setFiles(previous => ({ ...previous, isUploading:false }));
    //         })
    //         .catch((err) => {
    //             // inform the user
    //             console.error(err)
    //             removeFile(files.file.name);
    //         });
       
    // }

    return (
        <>
            <div className="file-card">

                <div className="file-inputs">
                    <input type="file" onChange={uploadHandler} key={files?.file?.name} />
                    <button>
                        <i>
                            <FontAwesomeIcon icon={faPlus} />
                        </i>
                        Upload
                    </button>
                </div>

                <p className="main">Supported files</p>
                <p className="info">PDF, JPG, PNG</p>

            </div>
            {/* <div> 
                
                    <button onClick={FileDone} className="file-done">
                      Done
                    </button>
            </div> */}
        </>
    )
}

export default FileUpload
