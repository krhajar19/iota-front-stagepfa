import axios from 'axios'
import React from 'react'
import FileItem from './../FileItem/FileItem'

const FileList = ({ files,removeFile}) => {
    const deleteFileHandler = (_name) => {
        axios.delete(`http://localhost:8081/upload?name=${_name}`)
            .then((res) => removeFile(_name))
            .catch((err) => console.error(err));

    console.log("delet file");
    }
    return (
        <ul className="file-list">
            {
                files?.file &&
                <FileItem
                    key={files.file.name}
                    files={files}
                    deleteFile={removeFile} />
            }
        </ul>
    )
}

export default FileList
